package com.analystplatform.seatbooker;

import com.analystplatform.seatbooker.models.ReservationConfirmation;
import com.analystplatform.seatbooker.models.Seat;
import com.analystplatform.seatbooker.models.SeatHold;
import com.analystplatform.seatbooker.services.SeatBookingService;
import org.apache.commons.validator.routines.EmailValidator;

import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <p>
 * Model classes already exist for Seat, SeatHold, and ReservationConfirmation.  Their definition and javadoc are included in the classpath jar.
 * <p>
 * All seat interactions should be achievable via the parent class's included SeatDao reference.  It can be accessed within SeatBookingEngine via:
 *
 * <pre>
 * this.getDao()
 * </pre>
 * <p>
 * The public methods on SeatDao include the following list.
 *
 * <pre>
 * // Returns all the seats
 * public Seat[][] getSeats();
 *
 * // Returns the number of rows in the venue
 * public int getNumberOfRows();
 *
 * // Returns the number of columns in the venue
 * public int getNumberOfColumns();
 *
 * // Get all the Seats being held, mapped to the IDs of their holds
 * public Map<Integer, SeatHold> getSeatHolds();
 *
 * // Removes all the seat holds of the given IDs
 * public void removeAllSeatHolds(List<Integer> seatHoldIds);
 *
 * // Holds a list of Seats for the given customer email address. Creates IDs and updates the in-memory data model with
 * // the held seats
 * public SeatHold newSeatHold(String customerEmail, List<Seat> seatsToHold);
 *
 * // Returns a seat hold for the given ID. Returns null if one wasn't found
 * public SeatHold getSeatHold(int id);
 *
 * // Removes a seat hold from the database of seat holds
 * public void removeSeatHold(int id);
 *
 * // Returns any expired seats into the pool of available seats.
 * public void reclaimExpiredHolds();
 *
 * // Returns the given confirmation for a valid id, null otherwise
 * public ReservationConfirmation getReservationConfirmation(String confirmationNumber);
 *
 * // Saves the given confirmation
 * public void saveReservationConfirmation(ReservationConfirmation confirmation);
 *
 * // Returns all reservation confirmations as a list
 * public List<ReservationConfirmation> getReservationConfirmations();
 * </pre>
 */
public class SeatBookingEngine extends SeatBookingService {
    // Give a flat seat list to make it easier to work with all seats at once
    private List<Seat> flatSeatList = Arrays.stream(getAllSeats()).flatMap(Arrays::stream).collect(Collectors.toList());
    private Map<Seat, Double> seatRanks;
    private ScheduledExecutorService seatHoldReclaimer = Executors.newSingleThreadScheduledExecutor();

    public SeatBookingEngine() {
        seatRanks = getRankedSeats();
    }

    /**
     * The number of seats that are neither held nor reserved
     *
     * @return the number of seats available in the venue
     */
    @Override
    public int numSeatsAvailable() {
        return getOpenSeats().size();
    }

    /**
     * Find and hold the best available seats for a customer.
     * <p>
     * Note: Customers who book together prefer to sit near each other whenever possible.  Seats are preferable nearest the stage.
     * </p>
     *
     * @param numSeats      the number of seats to find and hold
     * @param customerEmail unique identifier for the customer
     * @return a SeatHold object identifying the specific seats and related information
     */
    @Override
    public SeatHold findAndHoldSeats(int numSeats, String customerEmail) {
        if (!EmailValidator.getInstance().isValid(customerEmail)) {
            // It would be nice if we could validate and then re-prompt, but without access to the SeatBookingDriver
            // and with the relevant functions being private so extension is impossible, unfortunately we just have to
            // send a warning to the user and move on.
            System.err.println("WARNING: This appears to be an invalid email address!");
        }

        // Find the best seats in the house for the number of tickets requested. For the actual algorithm, see
        // the comment on the findBestSeats method below.
        List<Seat> bestSeats = new ArrayList<>(findBestSeats(getOpenSeats(), numSeats));

        // Make sure we mark each seat as held since the DAO doesn't do this for us
        bestSeats.forEach(seat -> seat.setHeld(true));

        // The app currently doesn't print out each set of seats as you select them, rather you have to select
        // the option to print out seat holds and find the seats you were assigned. This shows which seats are
        // being held, ordered by the row then column.
        bestSeats.sort(Comparator.comparing(Seat::getRow).thenComparing(Seat::getColumn));
        System.out.printf("\tSeats:\t%s%n%n", bestSeats.stream().map(seat ->
                String.format("(%d, %d)", seat.getRow(), seat.getColumn())
        ).collect(Collectors.joining(", ")));

        SeatHold seatHold = getDao().newSeatHold(customerEmail, bestSeats);
        scheduleExpireHolds();
        return seatHold;
    }

    ScheduledFuture scheduleExpireHolds() {
        return seatHoldReclaimer.schedule(getDao()::reclaimExpiredHolds, ApplicationConfig.MAX_SECONDS_TO_HOLD_SEATS, TimeUnit.SECONDS);
    }

    /**
     * Algorithm to find the best seats in the tightest clustered distance from one another based on the remaining
     * available seats in a venue. The algorithm is broken down below.
     *
     * There are two main concepts at play here and balancing them is a bit of a trick:
     *   1. The person booking the tickets would like all of the tickets to be as close together as possible
     *   2. These tickets also should be in the best seats that are still available
     *
     * So the way this algorithm works is it starts from a single point and works outward. That single point is chosen
     * as the seat with the highest rank remaining (see {@link #getRankedSeats}). From there, each "next best" seat is
     * checked to see if it is adjacent to that seat. If so, it's added, otherwise the next seat is checked. From there,
     * each additional seat is checked to see if it's adjacent to any seat already chosen in an attempt to make sure all
     * of the tickets form a single contiguous pattern.
     *
     * If we go through every seat in the venue and haven't found enough seats, then instead of looking for only seats
     * adjacent to ones already chosen, we step up to attempt to find any that are within a distance of "2", or
     * in other words, look a little further away. And then we keep going further and further outwards until we've
     * found enough seats.
     *
     * This algorithm isn't perfect, but it will almost always give seats that are adjacent and fairly high-value.
     *
     * @param seats The available seats
     * @param numSeats The number of seats to find
     * @return A list of the best seats in the house!
     */
    @SuppressWarnings("SuspiciousMethodCalls")
    List<Seat> findBestSeats(List<Seat> seats, int numSeats) {
        // First sort the incoming list of seats by the seat rank so the highest value seats are first.
        seats.sort(Comparator.comparing(seat -> seatRanks.get(seat)).reversed());

        List<Seat> bestSeats = new ArrayList<>();
        int maxDistance = 1;
        // The maximum distance any seat can be from any other seat is equivalent to the area of the venue.
        // So start at 1, attempting to find only adjacent seats, and then if we aren't able to fill the ticket
        // request with only adjacent seats (a contiguous pattern), then start stepping up to find ones that are
        // close, just not adjacent.
        while (maxDistance < (getDao().getNumberOfRows() * getDao().getNumberOfColumns())) {
            int i = 0;
            // Begin looping through the remaining seats (already sorted so the best seats are first)
            // We'll stop if we find enough seats or if we have looked at all of the seats
            while (bestSeats.size() < numSeats && i < seats.size()) {
                Seat currentSeat = seats.get(i);
                // If we currently don't have any seats, the "best seat" is our starting point.
                // Otherwise, if this seat is "close enough" to the rest of our seats, let's add it
                if (bestSeats.isEmpty() || isWithinMaxDistance(bestSeats, currentSeat, maxDistance)) {
                    bestSeats.add(currentSeat);
                    // Remove this seat so future iterations don't try to re-use the same seat
                    seats.remove(i);
                    // And now reset our counters since the next seat could potentially be adjacent to
                    // the seat we just added and may be higher value than the one we just added
                    i = 0;
                    maxDistance = 1;
                } else {
                    i++;
                }
            }

            if (bestSeats.size() == numSeats) break;

            maxDistance++;
        }

        return bestSeats;
    }

    /**
     * Determines if the given seat is within the maximum distance from at least one seat in the list of current seats.
     * Essentially this is to find out whether or not this seat is close enough to the "area" that the other seats
     * already encompass.
     *
     * @param currentSeats List of seats to check the distance of the given seat against
     * @param seat Seat to determine the distance from
     * @param maxDistance The maximum distance the given seat is allowed to be from any other seat given
     * @return true if within max distance, false otherwise
     */
    boolean isWithinMaxDistance(List<Seat> currentSeats, Seat seat, int maxDistance) {
        return currentSeats.stream().anyMatch(s -> getDistance(seat, s) <= maxDistance);
    }

    /**
     * Slightly different distance formula from the standard coordinate point distance calculation, used to determine
     * how far away one seat is from another seat. Because we are iterating through a "max distance", it's preferable
     * to use whole integers to represent distance and the standard distance formula will usually return a decimal,
     * and rounding it causes seats diagonal from one another to be counted as adjacent when we only want seats
     * orthogonally placed from one another to be counted as adjacent.
     *
     * See {@link Math#hypot} for the standard distance formula.
     *
     * @param seat1 Seat to start the distance from
     * @param seat2 Seat to calculate the distance to
     * @return A rounded integer of the distance between the two seats
     */
    int getDistance(Seat seat1, Seat seat2) {
        return Math.abs(seat1.getRow() - seat2.getRow()) + Math.abs(seat1.getColumn() - seat2.getColumn());
    }

    /**
     * Reserve unexpired seats held for a specific customer
     * <p>
     * Implementation note:
     *
     * <pre>
     * - If a 'hold' cannot be found for a given seatHoldId, return SeatBookingService.HOLD_NOT_FOUND_MESSAGE
     * - If a 'hold' doesn't exist for the given customerEmail, return SeatBookingService.HOLD_FOR_DIFFERENT_CUSTOMER_MESSAGE
     * </pre>
     *
     * @param seatHoldId    the seat hold identifier
     * @param customerEmail the email address of the customer to which the seat hold is assigned
     * @return a reservation confirmation code
     */
    @Override
    public String reserveSeats(int seatHoldId, String customerEmail) {
        if (!getDao().getSeatHolds().containsKey(seatHoldId)) {
            return SeatBookingService.HOLD_NOT_FOUND_MESSAGE;
        }

        SeatHold seatHold = getDao().getSeatHold(seatHoldId);

        if (!customerEmail.equalsIgnoreCase(seatHold.getEmail())) {
            return SeatBookingService.HOLD_FOR_DIFFERENT_CUSTOMER_MESSAGE;
        }

        List<Seat> seats = seatHold.getSeats();
        seats.forEach(seat -> seat.setReserved(true));

        ReservationConfirmation reservation = new ReservationConfirmation();
        reservation.setId(UUID.randomUUID().toString());
        reservation.setEmail(customerEmail);
        reservation.setSeats(seatHold.getSeats());

        getDao().saveReservationConfirmation(reservation);
        getDao().removeSeatHold(seatHoldId);

        return reservation.getId();
    }

    /**
     * Handy utility function to get a list of only the currently open seats.
     *
     * @return A list containing only open seats
     */
    List<Seat> getOpenSeats() {
        return flatSeatList.stream().filter(Seat::isOpen).collect(Collectors.toList());
    }

    /**
     * Algorithm that determines a perceived "value" of every seat in a venue based on its location. This is
     * pre-calculated at application start as opposed to each time a seat is retrieved to save time during
     * user interaction since the seat ranks obviously won't change.
     * <p>
     * Each seat is ranked based on how close it is to the stage and how close it is to the middle of the row.
     * This means that the best seats in the house are "Front and Center", whereas the worst seats are in the
     * back corners of the venue. Provided in the repository is an image that shows the distribution of values.
     * <p>
     * This ranking system utilizes a simple 0-10 rank based on the positioning. The first row is a perfect 10, and the
     * last row is an imperfect 0. Similarly, the middle column (or columns if there is an even number of columns) is
     * a perfect 10 and the columns on the edges are imperfect 0s.
     * <p>
     * Essentially the row distribution is normalized on a 0-10 scale with a simple proportion formula from Algebra 1:
     *
     * <pre>
     *    maxRows - row         rank
     *  -----------------  =  --------
     *       maxRows             10
     * </pre>
     * <p>
     * And of course you solve for rank by the formula of (maxRows - row) * 10 / maxRows.
     * <p>
     * The column formula is roughly the same except the median column is calculated and the values are normalized
     * against that column (using absolute value) and then subtracted from 10 (so that it's middle-out instead of
     * outside-in) to get the column value.
     * <p>
     * Then, for each seat, the row value and column value are averaged to get its final value.
     * <p>
     * With an example venue of 10 rows and 40 columns, the distribution is as follows:
     * <p>
     * 5.2 5.5 5.7 6.0 6.2 6.5 6.7 7.0 7.2 7.4 7.7 7.9 8.2 8.4 8.7 8.9 9.1 9.4 9.6 9.9 9.9 9.6 9.4 9.1 8.9 8.7 8.4 8.2 7.9 7.7 7.4 7.2 7.0 6.7 6.5 6.2 6.0 5.7 5.5 5.2
     * 4.7 5.0 5.2 5.5 5.7 6.0 6.2 6.5 6.7 6.9 7.2 7.4 7.7 7.9 8.2 8.4 8.6 8.9 9.1 9.4 9.4 9.1 8.9 8.6 8.4 8.2 7.9 7.7 7.4 7.2 6.9 6.7 6.5 6.2 6.0 5.7 5.5 5.2 5.0 4.7
     * 4.2 4.5 4.7 5.0 5.2 5.5 5.7 6.0 6.2 6.4 6.7 6.9 7.2 7.4 7.7 7.9 8.1 8.4 8.6 8.9 8.9 8.6 8.4 8.1 7.9 7.7 7.4 7.2 6.9 6.7 6.4 6.2 6.0 5.7 5.5 5.2 5.0 4.7 4.5 4.2
     * 3.7 4.0 4.2 4.5 4.7 5.0 5.2 5.5 5.7 5.9 6.2 6.4 6.7 6.9 7.2 7.4 7.6 7.9 8.1 8.4 8.4 8.1 7.9 7.6 7.4 7.2 6.9 6.7 6.4 6.2 5.9 5.7 5.5 5.2 5.0 4.7 4.5 4.2 4.0 3.7
     * 3.2 3.5 3.7 4.0 4.2 4.5 4.7 5.0 5.2 5.4 5.7 5.9 6.2 6.4 6.7 6.9 7.1 7.4 7.6 7.9 7.9 7.6 7.4 7.1 6.9 6.7 6.4 6.2 5.9 5.7 5.4 5.2 5.0 4.7 4.5 4.2 4.0 3.7 3.5 3.2
     * 2.7 3.0 3.2 3.5 3.7 4.0 4.2 4.5 4.7 4.9 5.2 5.4 5.7 5.9 6.2 6.4 6.6 6.9 7.1 7.4 7.4 7.1 6.9 6.6 6.4 6.2 5.9 5.7 5.4 5.2 4.9 4.7 4.5 4.2 4.0 3.7 3.5 3.2 3.0 2.7
     * 2.2 2.5 2.7 3.0 3.2 3.5 3.7 4.0 4.2 4.4 4.7 4.9 5.2 5.4 5.7 5.9 6.1 6.4 6.6 6.9 6.9 6.6 6.4 6.1 5.9 5.7 5.4 5.2 4.9 4.7 4.4 4.2 4.0 3.7 3.5 3.2 3.0 2.7 2.5 2.2
     * 1.7 2.0 2.2 2.5 2.7 3.0 3.2 3.5 3.7 3.9 4.2 4.4 4.7 4.9 5.2 5.4 5.6 5.9 6.1 6.4 6.4 6.1 5.9 5.6 5.4 5.2 4.9 4.7 4.4 4.2 3.9 3.7 3.5 3.2 3.0 2.7 2.5 2.2 2.0 1.7
     * 1.2 1.5 1.7 2.0 2.2 2.5 2.7 3.0 3.2 3.4 3.7 3.9 4.2 4.4 4.7 4.9 5.1 5.4 5.6 5.9 5.9 5.6 5.4 5.1 4.9 4.7 4.4 4.2 3.9 3.7 3.4 3.2 3.0 2.7 2.5 2.2 2.0 1.7 1.5 1.2
     * 0.7 1.0 1.2 1.5 1.7 2.0 2.2 2.5 2.7 2.9 3.2 3.4 3.7 3.9 4.2 4.4 4.6 4.9 5.1 5.4 5.4 5.1 4.9 4.6 4.4 4.2 3.9 3.7 3.4 3.2 2.9 2.7 2.5 2.2 2.0 1.7 1.5 1.2 1.0 0.7
     *
     * @return A Map of Seat -> Value pairs.
     */
    Map<Seat, Double> getRankedSeats() {
        int numRows = getDao().getNumberOfRows();
        int numColumns = getDao().getNumberOfColumns();
        // E.X. 40 cols: 40 / 2 = 20 + 0.5 = 20.5; 41 cols: 41 / 2 = 20.5 + 0.5 = 21
        double medianColumn = (double) (numColumns / 2) + 0.5;

        return flatSeatList.stream().collect(Collectors.toMap(Function.identity(), seat -> {
            // The row value is fairly simple. Take the number of rows minus the seat's row number, multiply it
            // by 10, and divide by the number of rows. The reason why you want to take the number of rows minus
            // the current row is so you end up with a proportion where the "first row" is the "best row", or in
            // other words, row 0 out of 10 is displayed as a 10/10 in the proportion.
            double rowValue = (double) (numRows - seat.getRow()) * 10 / numRows;

            // The column value takes a bit more calculation. The median has already been calculated, so you start
            // with the seat's column + 1 (to bring it on a 1-indexed scale, otherwise the calculation is off by 1),
            // then do the same thing as before, normalize it onto the same 0-10 scale. However, this normalizes it
            // such that the ending columns are the best instead of the middle columns, so take that score and subtract
            // it from 10 to get the final value.
            double colValue = 10 - Math.abs((medianColumn - (seat.getColumn() + 1)) * 10 / medianColumn);

            return (rowValue + colValue) / 2;
        }));
    }
}
