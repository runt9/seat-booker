package com.analystplatform.seatbooker;

public class MainApplication {

	public static void main(String... args) {
		System.out.println("Welcome to the event booking engine!");

		new SeatBookingDriver().run(new SeatBookingEngine());

		System.out.println("Thanks for using the booking engine!");
		System.exit(0);
	}

}
