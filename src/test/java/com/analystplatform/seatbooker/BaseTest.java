package com.analystplatform.seatbooker;

import com.analystplatform.seatbooker.models.Seat;
import org.junit.Before;

abstract class BaseTest {
    static final String TEST_EMAIL = "test@test.com";
    SeatBookingEngine engine;

    @Before
    public void setup() {
        this.engine = new SeatBookingEngine();
        // It would be great to mock the SeatDao here so we can mess with row/column numbers and whatnot,
        // but unfortunately it's not dependency injected and the getDao method on SeatBookingService is protected
    }

    int totalSeats() {
        return ApplicationConfig.ROWS * ApplicationConfig.COLUMNS;
    }

    Seat buildSeat(int row, int column) {
        Seat seat = new Seat();
        seat.setRow(row);
        seat.setColumn(column);
        return seat;
    }

    Seat getSeatAt(int row, int column) {
        return engine.getAllSeats()[row][column];
    }

    boolean seatIsPosition(Seat seat, int row, int column) {
        return seat.getRow() == row && seat.getColumn() == column;
    }
}
