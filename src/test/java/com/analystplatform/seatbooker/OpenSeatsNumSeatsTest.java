package com.analystplatform.seatbooker;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OpenSeatsNumSeatsTest extends BaseTest {
    @Test
    public void getOpenSeatsWithNoHeldSeatsReturnsAllSeats() {
        assertEquals(engine.getOpenSeats().size(), totalSeats());
    }

    @Test
    public void getOpenSeatsWithOneHeldSeatReturnsAllSeatsMinusOne() {
        engine.getOpenSeats().get(0).setHeld(true);
        assertEquals(engine.getOpenSeats().size(), totalSeats() - 1);
    }

    @Test
    public void getOpenSeatsWithOneReservedSeatReturnsAllSeatsMinusOne() {
        engine.getOpenSeats().get(0).setReserved(true);
        assertEquals(engine.getOpenSeats().size(), totalSeats() - 1);
    }

    @Test
    public void getNumSeatsAvailableWithNoHeldSeatsReturnsAllSeats() {
        assertEquals(engine.numSeatsAvailable(), totalSeats());
    }

    @Test
    public void getNumSeatsAvailableWithOneHeldSeatReturnsAllSeatsMinusOne() {
        engine.getOpenSeats().get(0).setHeld(true);
        assertEquals(engine.numSeatsAvailable(), totalSeats() - 1);
    }

    @Test
    public void getNumSeatsAvailableWithOneReservedSeatReturnsAllSeatsMinusOne() {
        engine.getOpenSeats().get(0).setReserved(true);
        assertEquals(engine.numSeatsAvailable(), totalSeats() - 1);
    }
}
