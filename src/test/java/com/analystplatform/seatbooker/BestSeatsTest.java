package com.analystplatform.seatbooker;

import com.analystplatform.seatbooker.models.Seat;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class BestSeatsTest extends BaseTest {
    @Test
    public void findBestSeatsReturnsFrontAndCenterForOneSeatRequested() {
        Seat bestSeat = engine.findBestSeats(engine.getOpenSeats(), 1).get(0);
        assertTrue(seatIsPosition(bestSeat, 0, 19));
    }

    @Test
    public void findBestSeatsReturnsBothFrontAndCenterForTwoSeatsRequested() {
        List<Seat> bestSeats = engine.findBestSeats(engine.getOpenSeats(), 2);

        assertTrue(seatIsPosition(bestSeats.get(0), 0, 19));
        assertTrue(seatIsPosition(bestSeats.get(1), 0, 20));
    }

    @Test
    public void findBestSeatsReturnsEightFrontAndTwoBehindForTenSeatsRequested() {
        List<Seat> bestSeats = engine.findBestSeats(engine.getOpenSeats(), 10);

        // Best seats in the front row
        assertTrue(seatIsPosition(bestSeats.get(0), 0, 19));
        assertTrue(seatIsPosition(bestSeats.get(1), 0, 20));
        assertTrue(seatIsPosition(bestSeats.get(2), 0, 18));
        assertTrue(seatIsPosition(bestSeats.get(3), 0, 21));
        assertTrue(seatIsPosition(bestSeats.get(4), 0, 17));
        assertTrue(seatIsPosition(bestSeats.get(5), 0, 22));

        // Best two seats in the second row
        assertTrue(seatIsPosition(bestSeats.get(6), 1, 19));
        assertTrue(seatIsPosition(bestSeats.get(7), 1, 20));

        // These two on the edge of the existing 6 taken on the front row are slightly higher rank than
        // the two seats on either side of the second row seats, so these should be next
        assertTrue(seatIsPosition(bestSeats.get(8), 0, 16));
        assertTrue(seatIsPosition(bestSeats.get(9), 0, 23));
    }

    @Test
    public void findBestSeatsReturnsSecondRowLeftOfHeldForOneSeatRequestedAfterTenSeatsHeld() {
        engine.findBestSeats(engine.getOpenSeats(), 10).forEach(seat -> seat.setHeld(true));
        List<Seat> bestSeats = engine.findBestSeats(engine.getOpenSeats(), 1);

        assertTrue(seatIsPosition(bestSeats.get(0), 1, 18));
    }
}
