package com.analystplatform.seatbooker;

import com.analystplatform.seatbooker.models.Seat;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MaxDistanceTest extends BaseTest {
    private List<Seat> buildCurrentSeats(Seat... seats) {
        return Arrays.stream(seats).collect(Collectors.toList());
    }

    @Test
    public void adjacentSeatIsWithinMaxDistanceOfOne() {
        List<Seat> currentSeats = buildCurrentSeats(buildSeat(0, 0));
        Seat nextSeat = buildSeat(0, 1);

        assertTrue(engine.isWithinMaxDistance(currentSeats, nextSeat, 1));
    }

    @Test
    public void nonAdjacentSeatIsNotWithinMaxDistanceOfOne() {
        List<Seat> currentSeats = buildCurrentSeats(buildSeat(0, 0));
        Seat nextSeat = buildSeat(0, 2);

        assertFalse(engine.isWithinMaxDistance(currentSeats, nextSeat, 1));
    }

    @Test
    public void adjacentSeatIsWithinMaxDistanceOfOneWithMultipleSeats() {
        List<Seat> currentSeats = buildCurrentSeats(buildSeat(0, 0), buildSeat(0, 1));
        Seat nextSeat = buildSeat(0, 2);

        assertTrue(engine.isWithinMaxDistance(currentSeats, nextSeat, 1));
    }

    @Test
    public void nonAdjacentSeatIsNotWithinMaxDistanceOfOneWithMultipleSeats() {
        List<Seat> currentSeats = buildCurrentSeats(buildSeat(0, 0), buildSeat(0, 1));
        Seat nextSeat = buildSeat(0, 3);

        assertFalse(engine.isWithinMaxDistance(currentSeats, nextSeat, 1));
    }

    @Test
    public void adjacentSeatIsWithinMaxDistanceOfOneAfterAddingSeat() {
        List<Seat> currentSeats = buildCurrentSeats(buildSeat(0, 0));
        Seat nextSeat = buildSeat(0, 2);

        assertFalse(engine.isWithinMaxDistance(currentSeats, nextSeat, 1));

        currentSeats.add(buildSeat(0, 1));

        assertTrue(engine.isWithinMaxDistance(currentSeats, nextSeat, 1));
    }

    @Test
    public void nonAdjacentSeatIsNotWithinMaxDistanceOfOneEvenAfterAddingSeat() {
        List<Seat> currentSeats = buildCurrentSeats(buildSeat(0, 0));
        Seat nextSeat = buildSeat(0, 3);

        assertFalse(engine.isWithinMaxDistance(currentSeats, nextSeat, 1));

        currentSeats.add(buildSeat(0, 1));

        assertFalse(engine.isWithinMaxDistance(currentSeats, nextSeat, 1));
    }
}
