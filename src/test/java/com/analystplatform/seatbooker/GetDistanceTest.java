package com.analystplatform.seatbooker;

import com.analystplatform.seatbooker.models.Seat;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GetDistanceTest extends BaseTest {
    @Test
    public void getDistanceBetweenAdjacentSeatsOnSameRowIsOne() {
        Seat seat1 = buildSeat(0, 0);
        Seat seat2 = buildSeat(0, 1);
        assertEquals(engine.getDistance(seat1, seat2), 1);
    }

    @Test
    public void getDistanceBetweenAdjacentSeatsOnSameColumnIsOne() {
        Seat seat1 = buildSeat(0, 0);
        Seat seat2 = buildSeat(1, 0);
        assertEquals(engine.getDistance(seat1, seat2), 1);
    }

    @Test
    public void getDistanceBetweenDiagonallyAdjacentIsTwo() {
        Seat seat1 = buildSeat(0, 0);
        Seat seat2 = buildSeat(1, 1);
        assertEquals(engine.getDistance(seat1, seat2), 2);
    }

    @Test
    public void getDistanceBetweenOppositeCornersIs48() {
        Seat seat1 = buildSeat(0, 0);
        Seat seat2 = buildSeat(9, 39);
        assertEquals(engine.getDistance(seat1, seat2), 48);
    }

    @Test
    public void getDistanceBetweenInvertedSeatDirectionIsStillPositive() {
        Seat seat1 = buildSeat(10, 15);
        Seat seat2 = buildSeat(0, 0);
        assertTrue(engine.getDistance(seat1, seat2) > 0);
    }
}
