package com.analystplatform.seatbooker;

import com.analystplatform.seatbooker.models.SeatHold;
import com.analystplatform.seatbooker.services.SeatBookingService;
import org.junit.Test;

import static org.junit.Assert.*;

public class SeatBookingEngineTest extends BaseTest {
    @Test
    public void findAndHoldSeatsHoldsBestSeatForOneSeatRequested() {
        SeatHold hold = engine.findAndHoldSeats(1, TEST_EMAIL);
        assertTrue(seatIsPosition(hold.getSeats().get(0), 0, 19));
    }

    @Test
    public void findAndHoldSeatsMarksSeatAsHeld() {
        SeatHold hold = engine.findAndHoldSeats(1, TEST_EMAIL);
        assertTrue(hold.getSeats().get(0).isHeld());
    }

    @Test
    public void cannotReserveForInvalidHoldId() {
        engine.findAndHoldSeats(1, TEST_EMAIL);
        assertEquals(engine.reserveSeats(-1, TEST_EMAIL), SeatBookingService.HOLD_NOT_FOUND_MESSAGE);
    }

    @Test
    public void cannotReserveForInvalidEmail() {
        int holdId = engine.findAndHoldSeats(1, TEST_EMAIL).getId();
        assertEquals(engine.reserveSeats(holdId, ""), SeatBookingService.HOLD_FOR_DIFFERENT_CUSTOMER_MESSAGE);
    }

    @Test
    public void canReserveForProperHoldId() {
        int holdId = engine.findAndHoldSeats(1, TEST_EMAIL).getId();
        assertNotEquals(engine.reserveSeats(holdId, TEST_EMAIL), SeatBookingService.HOLD_NOT_FOUND_MESSAGE);
        assertNotEquals(engine.reserveSeats(holdId, TEST_EMAIL), SeatBookingService.HOLD_FOR_DIFFERENT_CUSTOMER_MESSAGE);
    }

    @Test
    public void cannotReserveSameHoldIdTwice() {
        int holdId = engine.findAndHoldSeats(1, TEST_EMAIL).getId();
        engine.reserveSeats(holdId, TEST_EMAIL);
        assertEquals(engine.reserveSeats(holdId, TEST_EMAIL), SeatBookingService.HOLD_NOT_FOUND_MESSAGE);
    }

    @Test
    public void reserveMarksSeatsAsReserved() {
        SeatHold hold = engine.findAndHoldSeats(1, TEST_EMAIL);
        engine.reserveSeats(hold.getId(), TEST_EMAIL);
        assertTrue(hold.getSeats().get(0).isReserved());
    }

    @Test
    public void reserveGeneratesNewIds() {
        int hold1 = engine.findAndHoldSeats(1, TEST_EMAIL).getId();
        int hold2 = engine.findAndHoldSeats(1, TEST_EMAIL).getId();

        String id1 = engine.reserveSeats(hold1, TEST_EMAIL);
        String id2 = engine.reserveSeats(hold2, TEST_EMAIL);

        assertNotEquals(id1, id2);
    }
}
