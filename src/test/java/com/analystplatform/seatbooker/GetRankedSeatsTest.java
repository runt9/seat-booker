package com.analystplatform.seatbooker;

import com.analystplatform.seatbooker.models.Seat;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GetRankedSeatsTest extends BaseTest {
    @Test
    public void allSeatsAreRanked() {
        assertEquals(engine.getRankedSeats().size(), totalSeats());
    }

    @Test
    public void firstSeatRoundedIsRankedFive() {
        Seat seat = getSeatAt(0, 0);
        assertEquals(Math.round(engine.getRankedSeats().get(seat)), 5);
    }

    @Test
    public void frontAndCenterSeatRoundedIsRankedTen() {
        Seat seat = getSeatAt(0, 19);
        assertEquals(Math.round(engine.getRankedSeats().get(seat)), 10);
    }

    @Test
    public void backCornerSeatRoundedIsRankedOne() {
        Seat seat = getSeatAt(9, 0);
        assertEquals(Math.round(engine.getRankedSeats().get(seat)), 1);
    }

    @Test
    public void frontAndCenterSeatsAreBestSeats() {
        Seat seat = getSeatAt(0, 19);
        Map<Seat, Double> rankedSeats = engine.getRankedSeats();
        double bestValue = rankedSeats.get(seat);

        rankedSeats.forEach((s, r) -> {
            assertTrue(r <= bestValue);
        });
    }

    @Test
    public void backCornerSeatsAreWorstSeats() {
        Seat seat = getSeatAt(9, 0);
        Map<Seat, Double> rankedSeats = engine.getRankedSeats();
        double worstValue = rankedSeats.get(seat);

        rankedSeats.forEach((s, r) -> {
            assertTrue(r >= worstValue);
        });
    }
}
